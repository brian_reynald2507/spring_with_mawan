package com.brian.demo.day1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.brian.demo.day1.book.Buku;
import com.brian.demo.day1.book.TokoBuku;

@RestController
@RequestMapping("/pembukuan")
public class MyController {
	
	@Autowired 
	TokoBuku tokobuku;
	@Autowired 
	Buku buku;

	@RequestMapping("/toko")
	public TokoBuku tokobuku() {
		return new TokoBuku(new Buku(1,"Doraemon"));
	}

	@RequestMapping("/buku")
	public Buku buku(){
		return new Buku(1,"Doraemon");
	}
}
