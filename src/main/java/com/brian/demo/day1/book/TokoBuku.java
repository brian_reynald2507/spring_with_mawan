package com.brian.demo.day1.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TokoBuku {
	private int idToko;
	private Buku buku;
	private String alamat;
	
	@Autowired
	public TokoBuku(Buku buku) {
		this.buku = buku;
	}
	
	public TokoBuku() {
		
	}
	
	//setter getter idToko
	public int getIdToko() {
		return idToko;
	}
	public void setIdToko(int idToko) {
		this.idToko = idToko;
	}
	
	//setter getter buku
	public Buku getBuku() {
		return buku;
	}
	public void setBuku(Buku buku) {
		this.buku = buku;
	}
	
	//setter getter alamat
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
}
