package com.brian.demo.day1.book;

import org.springframework.stereotype.Component;

@Component
public class Buku {
	//declare variable
	private int idBuku;
	private String judulBuku;
	
	public Buku() {
		this.idBuku = 1;
		this.judulBuku = "Buku Doraemon";
	}
	
	//constructor
	public Buku(int idBuku, String judulBuku) {
		this.idBuku = idBuku;
		this.judulBuku = judulBuku;
	}
	
	//setter getter IdBuku
	public int getIdBuku() {
		return idBuku;
	}
	public void setIdBuku(int idBuku) {
		this.idBuku = idBuku;
	}
	
	//setter getter JudulBuku
	public String getJudulBuku() {
		return judulBuku;
	}
	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}
	
	@Override
	public String toString() {
		return "Buku [idBuku=" + idBuku + ", judulBuku=" + judulBuku + "]";
	}
	
}
