package com.brian.demo;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.brian.demo.DemoApplication;
import com.brian.demo.day1.book.Buku;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ApplicationContext appContext = SpringApplication.run(DemoApplication.class, args);
		
		Buku varBuku = new Buku(1,"buku doreaemon");
		System.out.println(appContext.getBean(Buku.class));
	}

}
